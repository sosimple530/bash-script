#!/bin/bash
#Bash script to install useful packages for a fresh ubuntu 20.04
#Internet connection and root access is required

#System update
sudo apt update
sudo apt upgrade -y

#Install git
sudo apt install git -y

#Install zsh
sudo apt install zsh -y

#Install ohmyzsh
yes | sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"

#Install hyperjs
wget -O hyper_3.0.2_amd64 https://releases.hyper.is/download/deb
sudo dpkg -i hyper_3.0.2_amd64
rm hyper_3.0.2_amd64

#Install powerlevel10k
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
sudo mv MesloLGS\ NF\ Regular.ttf /usr/local/share/fonts
fc-cache -f -v
#Need to manually select 'MesloLGS NF' fonts in terminal and hyperjs

#Install python packages
#Packages: pip, numpy, pandas beautifulsoup4, apscheduler, schedule, xmltodict 
sudo apt install python3-pip -y
alias python=python3
alias pip=pip3
pip install numpy --trusted-host pypi.org --trusted-host files.pythonhosted.org
pip install pandas --trusted-host pypi.org --trusted-host files.pythonhosted.org
pip install beautifulsoup4 --trusted-host pypi.org --trusted-host files.pythonhosted.org
pip install apscheduler --trusted-host pypi.org --trusted-host files.pythonhosted.org
pip install schedule --trusted-host pypi.org --trusted-host files.pythonhosted.org
pip install xmltodict --trusted-host pypi.org --trusted-host files.pythonhosted.org

#install curl and homebrew
sudo apt install curl -y
yes | /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> $HOME/.zshrc
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"

#Install nodejs and packages
#Packages: nvm, npm, jre, jdk, android-studio, watchman, react-native, netlify-cli, vercel
wget -O- https://deb.nodesource.com/setup_17.x | sudo -E bash
sudo apt-get install -y nodejs
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
sudo apt install default-jre -y
sudo apt install default-jdk -y
yes | sudo add-apt-repository ppa:maarten-fonville/android-studio
sudo apt update
sudo apt upgrade -y
sudo apt install android-studio -y
echo 'export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools' >> ~/.zshrc
echo 'export ANDROID_HOME=$HOME/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools' >> ~/.bashrc
brew install watchman
yes | npx react-native
sudo npm install netlify-cli -g
sudo npm i -g vercel

#Install aws
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
rm awscliv2.zip
rm -r aws

#Install mongodb
wget -qO - https://www.mongodb.org/static/pgp/server-5.0.asc | sudo apt-key add -
echo "deb [ arch=amd64,arm64 ] https://repo.mongodb.org/apt/ubuntu focal/mongodb-org/5.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-5.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org

#install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

#Install r and r studio server
sudo apt install dirmngr gnupg apt-transport-https ca-certificates software-properties-common -y
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
sudo add-apt-repository 'deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/'
sudo apt install r-base -y
sudo apt-get install gdebi-core
sudo apt update
wget https://download2.rstudio.org/server/bionic/amd64/rstudio-server-2021.09.2-382-amd64.deb
sudo dpkg -i rstudio-server-2021.09.2-382-amd64.deb
sudo apt install -yf
rm rstudio-server-2021.09.2-382-amd64.deb

#Install julia
JULIA_VERSION=$(curl -s "https://api.github.com/repos/JuliaLang/julia/releases/latest" | grep -Po '"tag_name": "v\K[0-9.]+')
JULIA_MINOR_VERSION=$(echo $JULIA_VERSION | grep -Po "^[0-9]+.[0-9]+")
curl -o julia.tar.gz "https://julialang-s3.julialang.org/bin/linux/x64/${JULIA_MINOR_VERSION}/julia-${JULIA_VERSION}-linux-x86_64.tar.gz"
sudo mkdir /opt/julia
sudo tar xf julia.tar.gz --strip-components=1 -C /opt/julia
sudo ln -s /opt/julia/bin/* /usr/local/bin
rm julia.tar.gz

#Install jupyter notebook
pip install notebook
export PATH=$PATH:$HOME/.local/bin

#Install anydesk
sudo bash -c "wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | apt-key add -"
sudo bash -c "echo 'deb http://deb.anydesk.com/ all main' > /etc/apt/sources.list.d/anydesk-stable.list"
sudo apt update
sudo apt install anydesk -y

#Install visual studio code
wget -qO- https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > packages.microsoft.gpg
sudo install -o root -g root -m 644 packages.microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64,arm64,armhf signed-by=/etc/apt/trusted.gpg.d/packages.microsoft.gpg] https://packages.microsoft.com/repos/code stable main" > /etc/apt/sources.list.d/vscode.list'
rm -f packages.microsoft.gpg
sudo apt install apt-transport-https
sudo apt update
sudo apt install code

#Install edge
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
sudo install -o root -g root -m 644 microsoft.gpg /etc/apt/trusted.gpg.d/
sudo sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/edge stable main" > /etc/apt/sources.list.d/microsoft-edge-dev.list'
sudo rm microsoft.gpg
sudo apt update
sudo apt install microsoft-edge-stable

sudo apt-get -f install
exit
